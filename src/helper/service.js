function service() {

    const baseApi = "http://localhost:3000/api";

    //introducir datos en el backend
    this.addUser=function(usuario){
        return axios.post(`${baseApi}/awardusers`,{
            firstname: usuario.firstname,
            lastname: usuario.lastname,
            picture: usuario.picture,
            email: usuario.email
        });
    }

    this.getUserAll = function () {
        return axios.get(`${baseApi}/awardusers`)
    }
}
var instance;
function singleFactory() {
    if (instance) {
        return instance;
    } else {
        instance = new service()
        return instance
    }
}

export default singleFactory();