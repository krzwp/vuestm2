import Vue from 'vue'
import Router from 'vue-router'
import indexaward from '@/components/indexaward'
import loginapp from '@/components/loginapp'
import award from '@/components/award'
import layoutAwardWellcome from '@/components/layoutAwardWellcome'
import awardWinners from '@/components/awardWinners'
// import pruebas from '@/components/pruebas'
// import compPadre from '@/components/compPadre'
// import registroapp from '@/components/registroapp'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'indexaward',
      component: indexaward,
      children: [
        {
          path: '/award',
          name: 'award',
          component: award
        },
        {
          path: '/wellcome',
          name: 'layoutAwardsWellcome',
          component: layoutAwardWellcome
        },
        {
          path: '/winners',
          name: 'winners',
          component: awardWinners,
        }
      ]
    }
    // {
    //   path: '/login',
    //   name: 'login',
    //   component: loginapp
    // },
    // {
    //   path: '/registro',
    //   name: 'register',
    //   component: registroapp
    // },
    // {
    //   path: '/pruebas',
    //   name: 'pruebas',
    //   component: pruebas
    // },
    // {
    //   path: '/awards',
    //   name: 'layoutAwards',
    //   component: layoutAwards
    // },
    // {
    //   path:"/padre",
    //   name:"padre",
    //   component:compPadre 
    // }
  ]
})
