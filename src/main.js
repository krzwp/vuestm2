// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Element from 'element-ui'
import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon'
import main from './assets/main.scss'
import axios from 'axios'
// 
Vue.config.productionTip = false

Vue.component('icon', Icon)
Vue.use(Element)
window.axios=require('axios')

// Vue.use(VeeValidate)
// Vue.use(axios)
/* eslint-dimport Element from 'element-ui'

Vue.use(Element)isable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
